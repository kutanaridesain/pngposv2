﻿using System;
using System.Linq;
using System.IO;
using System.IO.IsolatedStorage;
using System.Collections.Generic;
using Microsoft.LightSwitch;
using Microsoft.LightSwitch.Framework.Client;
using Microsoft.LightSwitch.Presentation;
using Microsoft.LightSwitch.Presentation.Extensions;
namespace LightSwitchApplication
{
    public partial class StoresListDetail
    {
        partial void Coba_Execute()
        {
            // Write your code here.
            this.Stores.AddAndEditNew();
            this.OpenModalWindow("WindowCoba");

        }

        partial void StoresListDetail_Saving(ref bool handled)
        {
            // Write your code here.
            //Console.WriteLine("coba aja");
            this.ShowMessageBox("coba aja");
        }

        partial void Simpan_Execute()
        {
            // Write your code here.
            //this.DataWorkspace.posData.Stores.AddNew();
            this.Stores.AddNew();
            this.Save();
        }
    }
}
